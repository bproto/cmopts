# CMOPTS: Common CMake Project Options

`cmopts` provides a CMake module that is used by the C++-based projects developed by me ([Bytes Mess](@b110011)).
This module helps to control the quality and consistency of such projects' build systems.

While the driving force of this module is to reduce duplication in CMake scripts across projects developed by me ([Bytes Mess](@b110011)), it is intended to be useful for any software that uses the CMake build system.

This module also may serve as a useful reference for setting up a modern CMake build system using good practices.

**Table of contents** (generated with [markdown-toc](http://ecotrust-canada.github.io/markdown-toc/))

- [Install](#install)
  - [Prerequisites](#prerequisites)
    - [System packages](#system-packages)
  - [Building from source](#building-from-source)
- [Usage](#usage)
- [Folder Structure](#folder-structure)
- [License](#license)

## Install

### Prerequisites

#### System packages

The only system prerequisite of `cmopts` is `cmake` package.

Ubuntu users can install `cmake` package with the official package manager:

```bash
$ sudo apt install cmake
```

### Building from source

To build and install from source, you can clone the repo and use cmake to install the modules as though this is a regular cmake project:

```bash
$ git clone https://gitlab.com/bproto/cmopts.git
$ cd cmopts
# local install:
$ cmake -S. -Bbuild -DCMAKE_INSTALL_PREFIX=/path/to/install/dir
$ cmake --build build --target install
# system wide install:
$ cmake -S. -Bbuild
$ sudo cmake --build build --target install
```

Replace `/path/to/install/dir` to whatever directory you want to install this package to.

## Usage

To use `cmopts`, add the following to your `CMakeLists.txt`:

```cmake
find_package(cmopts REQUIRED NO_MODULE)
```

You can then just use methods provided by `cmopts` as needed.
For example:

```cmake
add_library("${PROJECT_NAME}" STATIC ${HEADERS} ${SOURCE})
add_executable("${PROJECT_UNIT_TESTS}" ${PROJECT_UNIT_TESTS_SOURCES})

# Setup target with common CMake, compiler and linters settings. Also CMOPTS_*
# options which were passed during configuration step will be taken into account.
cmopts_setup_target("${PROJECT_NAME}")
cmopts_setup_target("${PROJECT_UNIT_TESTS}")

# Setup test target with common CMake, compiler and linters settings.
# Make it a part of specified test category.
cmopts_setup_test("${PROJECT_UNIT_TESTS}"
  NAME
    "My::Awesome::Project::UnitTests"
  UNIT_TEST
)

# Link to "system" libraries.
cmopts_target_link_system_libraries("${PROJECT_NAME}"
  PUBLIC
    Boost::headers
)

# Generate test coverage report
cmopts_setup_coverage(
    EXCLUDE
        ".*/tests/.*"
)
```

## Folder Structure

- `docs`: a comprehensive project documentation.
- `example`: a fake project that is used to test `cmopts`.
- `module`: cmake module that get installed by this package.
- `package`: template files for producing the config-files of `cmopts`; these are only used internally.

## Acknowledgment

I would like to express my deepest gratitude to the creators and contributors of the [gui_starter_template][gst] (former `cpp_starter_project`) project, upon which this project was initially based.

I would also like to thank the creators and contributors of the [project_options][apo] repository, the successor of the CMake part of the [gui_starter_template][gst] project.
Their efforts have allowed me to greatly decrease complexity of this project.

Thank you to both projects, as they have shown me ([Bytes Mess](@b110011)) how to cook and use CMake in all different ways!

[apo]: https://github.com/aminya/project_options
[gst]: https://github.com/cpp-best-practices/gui_starter_template

## License

This library is licensed under [Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0).
