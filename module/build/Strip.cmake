# Copyright 2023-2024 Bytes Mess <b110011@pm.me>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Removes symbols and sections from generated executable if it's a release-like
# build. See more here:
#   - https://github.com/leetal/ios-cmake/issues/65
#   - https://stackoverflow.com/questions/38675403
#   - https://www.linuxfixes.com/2022/02/solved-hiding-symbol-names-in-library.html
# For debug-like build consult below links:
#   - https://medium.com/@ffugavr/7554868d51ea
# Based on
#   - https://stackoverflow.com/questions/53692348.
function(_cmopts_strip _target)
    if(CMAKE_BUILD_TYPE MATCHES ".*Deb.*" AND CMAKE_STRIP)
        get_target_property(target_type "${_target}" TYPE)
        string(TOLOWER "${target_type}" target_type)
        string(REPLACE "_" " " target_type "${target_type}")

        add_custom_command(
            TARGET
                "${_target}"
            DEPENDS
                "${_target}"
            COMMAND
                ${CMAKE_STRIP}
            ARGS
                # List of possible PLATFORM_ID values:
                #   - https://gitlab.kitware.com/cmake/cmake/-/issues/21489
                --strip-all $<$<PLATFORM_ID:Darwin>:-x> $<TARGET_FILE:${_target}>
            POST_BUILD
            COMMENT
                "Striping '${_target}' resulting ${target_type}."
        )
    endif()
endfunction()
