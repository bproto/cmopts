# Copyright 2022-2024 Bytes Mess <b110011@pm.me>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# See below links for implementation ideas:
#   - https://github.com/swift-nav/cmake
#   - https://stackoverflow.com/questions/9303711
#   - https://stackoverflow.com/questions/26498089
#   - https://stackoverflow.com/questions/53141276
# See below links for CTest reference:
#   - https://cmake.org/cmake/help/latest/manual/ctest.1.html
# See below links for Valgrind's MemCheck tool reference:
#   - https://valgrind.org/docs/manual/manual-core.html
#   - https://valgrind.org/docs/manual/mc-manual.html

option(CMOPTS_ENABLE_VALGRING "Enable static analysis with valgrind" OFF)

set(VALGRING_OPTIONS
    # Core
    --error-exitcode=1
    --num-callers=20
    --trace-children=yes
    --track-fds=yes
    # MemCheck
    --tool=memcheck
    --leak-check=full
    --show-reachable=yes
)
set(CMOPTS_VALGRING_OPTIONS "${VALGRING_OPTIONS}" CACHE STRING "Choose the type of CppCheck report." FORCE)

if(CMOPTS_ENABLE_VALGRING)
    find_program(MEMORYCHECK_COMMAND valgrind)
    if(NOT MEMORYCHECK_COMMAND)
        message(SEND_ERROR "valgrind requested but executable not found")
    endif()

    string(REPLACE ";" " " MEMORYCHECK_COMMAND_OPTIONS "${CMOPTS_VALGRING_OPTIONS}")

    if(CTEST_MEMORYCHECK_SUPPRESSIONS_FILE)
        # NOTE(b110011): Nothing to do here.
    elseif(EXISTS "${CMAKE_SOURCE_DIR}/tools/valgrind.supp")
        set(MEMORYCHECK_SUPPRESSIONS_FILE "${CMAKE_SOURCE_DIR}/tools/valgrind.supp")
    elseif(EXISTS "${CMAKE_SOURCE_DIR}/.valgrind.supp")
        set(MEMORYCHECK_SUPPRESSIONS_FILE "${CMAKE_SOURCE_DIR}/.valgrind.supp")
    endif()

    set(MEMORYCHECK_TYPE "Valgrind")

    include(CTest)
endif()
