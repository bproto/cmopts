#!/usr/bin/env python3

# Copyright 2020-2024 Bytes Mess <b110011@pm.me>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from argparse import ArgumentParser
from dataclasses import dataclass
from pathlib import Path
from xml.etree import ElementTree as XmlTree
from xml.etree.ElementTree import Element as XmlElement
from sys import argv as script_args


@dataclass(frozen=True)
class CppCheckError:
    error_id: str
    file: str
    line: int
    column: int


def _remove_duplicates(root: XmlElement):
    visited = set()

    errors = root.find('./errors')
    for error in errors.findall('error'):
        error_id=error.get('id')
        for location in error.findall('./location'):
            e = CppCheckError(
                error_id=error_id,
                file=location.get('file'),
                line=location.get('line'),
                column=location.get('column')
            )

            if e in visited:
                error.remove(location)
            else:
                visited.add(e)

        if len(error.findall('./location')) == 0:
            errors.remove(error)


def _patch(path):
    tree = XmlTree.parse(path)
    root = tree.getroot()

    _remove_duplicates(root)

    with open(path, 'wb') as f:
        tree.write(f)


def _get_cli_arg_parser():
    parser = ArgumentParser(
        description='Remove duplicated entries from CppCheck XML report'
    )
    parser.add_argument(
        'file',
        type=Path,
        help='File path to CppCheck XML report'
    )
    return parser


if __name__ == '__main__':
    parser = _get_cli_arg_parser()
    args = parser.parse_args(script_args[1:])

    _patch(args.file)
