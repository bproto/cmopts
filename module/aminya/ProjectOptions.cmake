# Copyright 2023-2024 Bytes Mess <b110011@pm.me>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#
# Fetch aminya's project options
#

include(FetchContent)

FetchContent_Declare(_aminya_project_options
    GIT_REPOSITORY
        https://github.com/aminya/project_options.git
    GIT_TAG
        575d00a575d3b390344c2a426ad4cb1092f12e45 # v0.27.2
)
FetchContent_MakeAvailable(_aminya_project_options)

include("${_aminya_project_options_SOURCE_DIR}/Index.cmake")

#
# Setup aminya's project options
#

macro(_cmopts_bind_options)
    cmake_parse_arguments(ARGS "" "AMINYA;CMOPTS;DESCRIPTION" "" ${ARGN})

    if(ARGS_UNPARSED_ARGUMENTS)
        option(CMOPTS_${ARGS_UNPARSED_ARGUMENTS} "${ARGS_DESCRIPTION}" OFF)
        if(${CMOPTS_${ARGS_UNPARSED_ARGUMENTS}})
            set("${ARGS_UNPARSED_ARGUMENTS}" "${ARGS_UNPARSED_ARGUMENTS}")
        endif()
    else()
        option(CMOPTS_${ARGS_CMOPTS} "${ARGS_DESCRIPTION}" OFF)
        if(${CMOPTS_${ARGS_CMOPTS}})
            set("${ARGS_AMINYA}" "${ARGS_AMINYA}")
        endif()
    endif()
endmacro()

_cmopts_bind_options(DISABLE_EXCEPTIONS
    DESCRIPTION
        "Disable exceptions (no-exceptions and no-unwind-tables)"
)
_cmopts_bind_options(DISABLE_RTTI
    DESCRIPTION
        "Disable RTTI"
)
_cmopts_bind_options(ENABLE_CACHE
    DESCRIPTION
        "Enable ccache on Unix"
)
_cmopts_bind_options(
    AMINYA
        ENABLE_INCLUDE_WHAT_YOU_USE
    CMOPTS
        ENABLE_IWYU
    DESCRIPTION
        "Enable include-what-you-use analysis during compilation"
)
_cmopts_bind_options(
    AMINYA
        ENABLE_INTERPROCEDURAL_OPTIMIZATION
    CMOPTS
        ENABLE_LTO
    DESCRIPTION
        "Enable whole-program optimization (e.g. LTO)"
)
_cmopts_bind_options(ENABLE_NATIVE_OPTIMIZATION
    DESCRIPTION
        "Enable the optimizations specific to the build machine (e.g. SSE4_1, AVX2, etc.)"
)
_cmopts_bind_options(ENABLE_SANITIZER_ADDRESS
    DESCRIPTION
        "Make memory errors into hard runtime errors (windows/linux/macos)"
)
_cmopts_bind_options(ENABLE_SANITIZER_LEAK
    DESCRIPTION
        "Make memory leaks into hard runtime errors"
)
_cmopts_bind_options(ENABLE_SANITIZER_MEMORY
    DESCRIPTION
        "Make other memory errors into runtime errors"
)
_cmopts_bind_options(ENABLE_SANITIZER_THREAD
    DESCRIPTION
        "Make thread race conditions into hard runtime errors"
)
_cmopts_bind_options(ENABLE_SANITIZER_UNDEFINED_BEHAVIOR
    DESCRIPTION
        "Make certain types (numeric mostly) of undefined behavior into runtime errors"
)
_cmopts_bind_options(
    AMINYA
        ENABLE_BUILD_WITH_TIME_TRACE
    CMOPTS
        ENABLE_TIME_TRACE
    DESCRIPTION
        "Generates report of where compile-time is spent"
)

option(CMOPTS_LINKER "Choose a specific linker (e.g. lld, gold, bfd)" OFF)
if(${CMOPTS_LINKER})
    set(LINKER "LINKER ${CMOPTS_LINKER}")
endif()

project_options( # PROVIDER: aminya
    ${DISABLE_EXCEPTIONS}
    ${DISABLE_RTTI}
    ${ENABLE_CACHE}
    ${ENABLE_CLANG_TIDY}
    ${ENABLE_CPPCHECK}
    ${ENABLE_BUILD_WITH_TIME_TRACE}
    ${ENABLE_INCLUDE_WHAT_YOU_USE}
    ${ENABLE_INTERPROCEDURAL_OPTIMIZATION}
    ${ENABLE_NATIVE_OPTIMIZATION}
    ${ENABLE_SANITIZER_ADDRESS}
    ${ENABLE_SANITIZER_LEAK}
    ${ENABLE_SANITIZER_MEMORY}
    ${ENABLE_SANITIZER_THREAD}
    ${ENABLE_SANITIZER_UNDEFINED_BEHAVIOR}
    ENABLE_VS_ANALYSIS
    ${LINKER}
    PREFIX
        "cmopts"
    WARNINGS_AS_ERRORS
)
