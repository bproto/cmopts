# Copyright 2023-2024 Bytes Mess <b110011@pm.me>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

_cmopts_include(CodeCoverage)

macro(_cmopts_append_coverage_compiler_flags _target)
    append_coverage_compiler_flags_to_target("${_target}") # PROVIDER: CodeCoverage
endmacro()

function(cmopts_setup_coverage)
    cmake_parse_arguments(ARGS
        ""
        "BASE_DIRECTORY;COMMAND"
        "DEPENDS;EXCLUDE"
        ${ARGN}
    )

    # Make sure the user doesn't play dirty with symlinks
    get_filename_component(global_bin_dir "${CMAKE_BINARY_DIR}" REALPATH)
    get_filename_component(local_bin_dir "${PROJECT_BINARY_DIR}" REALPATH)

    # Allow only function call in top-level CMake script
    if(NOT "${global_bin_dir}" STREQUAL "${local_bin_dir}")
        message(FATAL_ERROR "Coverage can be setup only in top-level CMake script!")
    endif()

    set(target_name coverage)
    if(ARGS_UNPARSED_ARGUMENTS)
        set(target_name "${ARGS_UNPARSED_ARGUMENTS}")
    endif()

    if(NOT ARGS_EXECUTABLE)
        include(ProcessorCount)
        ProcessorCount(processor_count)

        set(ARGS_EXECUTABLE "ctest -j ${processor_count}")
    endif()

    setup_target_for_coverage_gcovr_html( # PROVIDER: CodeCoverage
        BASE_DIRECTORY
            "${ARGS_BASE_DIRECTORY}"
        DEPENDENCIES
            ${ARGS_DEPENDS}
        EXCLUDE
            ${ARGS_EXCLUDE}
        EXECUTABLE
            ${ARGS_COMMAND}
        NAME
            "${target_name}"
    )
endfunction()
