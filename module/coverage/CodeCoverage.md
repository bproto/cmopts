## Licenses

The modules that are written by [Ryan Pavlik](https://github.com/rpavlik/cmake-modules) are all subject to this license:

> Copyright Iowa State University 2009-2011
>
> Distributed under the Boost Software License, Version 1.0.
>
> (See accompanying file `LICENSE_1_0.txt` or copy at
> <http://www.boost.org/LICENSE_1_0.txt>)

Modules based on those included with CMake as well as modules added by me (Lars Bilke) are under the OSI-approved **BSD** license, which is included in each of those modules.
A few other modules are modified from other sources - when in doubt, look at the .cmake.

## Important License Note

If you find this file inside of another project, rather at the top-level directory, you're in a separate project that is making use of these modules.
That separate project can (and probably does) have its own license specifics.

## Acknowledgment

Taken from [bilke/cmake-modules](https://github.com/bilke/cmake-modules) repository.
