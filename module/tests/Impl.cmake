# Copyright 2020-2024 Bytes Mess <b110011@pm.me>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

enable_testing()

function(cmopts_add_tests)
    if(${ARGC} EQUAL 0)
        set(dir "tests")
    else()
        set(dir "${ARGV0}")
    endif()

    add_subdirectory("${dir}")
endfunction()

function(cmopts_setup_test _target)
    set(options BENCHMARKS COMPONENT_TESTS INTEGRATION_TESTS SYSTEM_TESTS UNIT_TESTS)
    cmake_parse_arguments(ARGS "${options}" "NAME" "" ${ARGN})

    # Check that target hasn't been already added to some category of tests.
    get_target_property(is_test_type_exists "${_target}" LABELS)
    if(NOT "${is_test_type_exists}" STREQUAL "is_test_type_exists-NOTFOUND")
        message(FATAL_ERROR "'cmopts_setup_test' accepts only one type per test target!")
    endif()

    # Check that user has called function only with one category of tests.
    foreach(option IN LISTS options)
        if(ARGS_${option})
            if(test_type)
                message(FATAL_ERROR "'cmopts_setup_test' accepts only one type per test target!")
            endif()

            set(test_type "${ARGS_${option}}")
        endif()
    endforeach()

    if(NOT test_type)
        message(FATAL_ERROR "'cmopts_setup_test' must be called with type of test target!")
    endif()

    # Add the test to the CTest registry.
    add_test(NAME "${ARGS_NAME}" COMMAND "${_target}")

    set_target_properties("${_target}"
        PROPERTIES
            _CMOPTS_TEST_NAME "${ARGS_NAME}"
    )

    # Add this test to a requested category of tests.
    set_tests_properties("${ARGS_NAME}"
        PROPERTIES
            LABELS "${test_type}"
    )

    # Don't ask how I came to this, but helps to show function names in stacktrace.
    # https://stackoverflow.com/questions/10599038
    set_target_properties("${_target}"
        PROPERTIES
            ENABLE_EXPORTS ON
    )
endfunction()

function(cmopts_test_disable_analysis _target)
    cmake_parse_arguments(ARGS "ALL;MEMCHECK" "" "" ${ARGN})
    get_property(test_name TARGET "${_target}" PROPERTY _CMOPTS_TEST_NAME)

    # if(ARGS_MEMCHECK)
        set_tests_properties("${test_name}" PROPERTIES LABELS DISABLE_MEMCHECK)
    # endif()
endfunction()
