# Copyright 2020-2024 Bytes Mess <b110011@pm.me>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

function(cmopts_setup_target _target)
    cmake_parse_arguments(ARGS "NO_CHECKS;STRIP" "" "" ${ARGN})

    set_target_properties("${_target}"
        PROPERTIES
            CXX_STANDARD_REQUIRED ON
            CXX_EXTENSIONS OFF
    )

    if(NOT CMAKE_BUILD_TYPE MATCHES ".*Deb.*")
        set_target_properties("${_target}"
            PROPERTIES
                CXX_VISIBILITY_PRESET hidden
                VISIBILITY_INLINES_HIDDEN ON
        )
    elseif(0)
        # Seems like this doesn't help to show function names in stacktrace.
        # https://stackoverflow.com/questions/52583544
        # https://stackoverflow.com/questions/62414294
        set_target_properties("${_target}"
            PROPERTIES
                ENABLE_EXPORTS ON
                POSITION_INDEPENDENT_CODE OFF
        )
    endif()

    # Make all necessary preparations for Windows compilers
    # FMI: https://gitlab.kitware.com/cmake/cmake/-/issues/21489
    if (CMAKE_SYSTEM_NAME STREQUAL "Windows")
        cmopts_setup_windows_target("${_target}")
    endif()

    target_link_libraries("${_target}"
        PRIVATE
            cmopts_project_options
    )

    if(NOT ARGS_NO_CHECKS)
        target_link_libraries("${_target}"
            PRIVATE
                cmopts_project_warnings
        )
    endif()

    if(ARGS_STRIP)
        _cmopts_strip("${_target}")
    endif()

    _cmopts_append_coverage_compiler_flags("${_target}")
endfunction()

function(cmopts_target_disable_analysis _target)
    cmake_parse_arguments(ARGS "ALL;IWYU;VSA" "" "" ${ARGN})

    if(ARGS_ALL OR ARGS_IWYU)
        target_disable_include_what_you_use("${_target}") # PROVIDER: aminya
    endif()

    if(ARGS_ALL OR ARGS_VSA)
        target_disable_vs_analysis("${_target}") # PROVIDER: aminya
    endif()
endfunction()

function(cmopts_target_link_system_libraries _target)
    cmake_parse_arguments(ARGS "" "" "INTERFACE;PUBLIC;PRIVATE" ${ARGN})
    target_link_system_libraries("${_target}" # PROVIDER: aminya
        INTERFACE
            ${ARGS_INTERFACE}
        PUBLIC
            ${ARGS_PUBLIC}
        PRIVATE
            ${ARGS_PRIVATE}
    )
endfunction()
