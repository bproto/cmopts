# CMOPTS: Example

This is a fake project that is used to test `cmopts`.

**Table of contents** (generated with [markdown-toc](http://ecotrust-canada.github.io/markdown-toc/))

- [Build](#build)
  - ["Local"](#local)
  - ["System"](#system)
- [License](#license)

## Build

### "Local"

You can generate documentation for an "example" project using a top-level CMake project, which is located in the root of the repo.

In order to do so, you need to run following commands:

```bash
# pwd: root of the repo.
$ cmake -S. -Bbuild -DCMOPTS_EXAMPLE=ON
$ cmake --build build
```

### "System"

You can generate documentation using only "example" project to emulate "real"-like usage.

In order to do so, first you need to install `cmopts` system-wide by running following commands:

```bash
# pwd: root of the repo.
$ cmake -S. -Bbuild
$ sudo cmake --build build --target install
```

Then you need to generate HTML documentation using only an "example project":

```bash
# pwd: root of the repo.
$ cmake -Sexample -Bbuild-example
$ cmake --build ./build-example
```

## License

This library is licensed under [Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0).
