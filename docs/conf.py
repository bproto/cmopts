# Copyright 2021-2024 Bytes Mess <b110011@pm.me>.
#
# CppDoc: CMake Module for an easy generation of C++ documentation.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

import datetime
import os

# -- Project information -----------------------------------------------------

author = "Bytes Mess"
project = "CMOPTS"

copyright = f"2021-{datetime.datetime.now().year}, Bytes Mess"  # noqa: A001

# -- General configuration ---------------------------------------------------

extensions = ["sphinx-prompt"]

exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]

# -- Options for HTML output -------------------------------------------------


def _is_ci_env() -> bool:
    return os.getenv("CI", False)


def _generate_last_updated_fmt() -> str:
    if not _is_ci_env():
        return ""

    branch = os.environ["CI_COMMIT_REF_NAME"]
    commit_sha = os.environ["CI_COMMIT_SHORT_SHA"]

    return f"{commit_sha} ({branch}), %y-%m-%d %H:%M:%S"


html_copy_source = False

html_last_updated_fmt = _generate_last_updated_fmt()

html_theme = "furo"
html_theme_options = {
    "footer_icons": [
        {
            "name": "GitLab",
            "url": os.getenv("CI_PROJECT_URL"),
            "html": """
                <svg stroke="currentColor" fill="currentColor" stroke-width="0" viewBox="0 0 512 512" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg">
                    <path d="M494.07 281.6l-25.18-78.08a11 11 0 00-.61-2.1l-50.5-156.94a20.08 20.08 0 00-19.17-13.82 19.77 19.77 0 00-18.95 13.94l-48.14 149.55h-152L131.34 44.59a19.76 19.76 0 00-18.86-13.94h-.11a20.15 20.15 0 00-19.12 14L42.7 201.73c0 .14-.11.26-.16.4l-25.63 79.48a29.15 29.15 0 0010.44 32.46l221.44 162.41a11.25 11.25 0 0013.38-.07l221.48-162.34a29.13 29.13 0 0010.42-32.47m-331-64.51l61.73 191.76L76.63 217.09m209.64 191.8l59.19-183.84 2.55-8h86.52L300.47 390.44M398.8 59.31l43.37 134.83h-86.82M324.16 217l-43 133.58-25.66 79.56L186.94 217M112.27 59.31l43.46 134.83H69M40.68 295.58a6.19 6.19 0 01-2.21-6.9l19-59 139.61 180.59m273.26-114.69L313.92 410.22l.52-.69L453.5 229.64l19 59a6.2 6.2 0 01-2.19 6.92" />
                </svg>
            """,
            "class": "",
        },
        {
            "name": os.getenv("CI_PROJECT_ROOT_NAMESPACE"),
            "url": f"https://{os.getenv('CI_PROJECT_ROOT_NAMESPACE')}.{os.getenv('CI_PAGES_DOMAIN')}",
            "html": """
                <svg stroke="currentColor" fill="currentColor" stroke-width="0" viewBox="0 0 24 24" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg">
                    <path fill="none" d="M0 0h24v24H0V0z" />
                    <path d="M12 7V3H2v18h20V7H12zM6 19H4v-2h2v2zm0-4H4v-2h2v2zm0-4H4V9h2v2zm0-4H4V5h2v2zm4 12H8v-2h2v2zm0-4H8v-2h2v2zm0-4H8V9h2v2zm0-4H8V5h2v2zm10 12h-8v-2h2v-2h-2v-2h2v-2h-2V9h8v10zm-2-8h-2v2h2v-2zm0 4h-2v2h2v-2z" />
                </svg>
            """,
            "class": "",
        },
    ],
    "source_repository": os.getenv("CI_PROJECT_URL"),
    "source_branch": os.getenv("CI_COMMIT_REF_NAME"),
    "source_directory": "docs/",
}

if not _is_ci_env():
    html_theme_options = {}

html_title = project
