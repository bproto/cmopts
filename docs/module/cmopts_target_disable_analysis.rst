##############################
cmopts_target_disable_analysis
##############################

Disable requested analysis for a given target.

.. code-block:: cmake

   cmopts_target_disable_analysis(<target> <tools...>)

.. note::
   It is worth to mention that this method is implemented on `aminya's project_options CMake module <https://github.com/aminya/project_options>`__ side and ``cmopts`` package simply provides a convenience wrapper for following methods:

   - ``target_disable_static_analysis``,
   - ``target_disable_include_what_you_use``,
   - ``target_disable_vs_analysis``.

Arguments
---------

.. _add_executable: https://cmake.org/cmake/help/latest/command/add_executable.html
.. _add_library: https://cmake.org/cmake/help/latest/command/add_executable.html
.. _ALIAS: https://cmake.org/cmake/help/latest/manual/cmake-buildsystem.7.html#alias-targets

The named ``<target>`` must have been created by a command such as `add_executable`_ or `add_library`_ and must not be an `ALIAS`_ target.

If ``ALL`` is passed as ``<tools>``, all available analysis will be disabled for a given target.

If you want a more granular control over what analysis to be disabled, you can use one or more of the following options for ``<tools>``:

``IWYU``
  Disable generation of `coverage <https://include-what-you-use.org>`__ report for a given target.

``IWYU``
  Disable all `include-what-you-use <https://include-what-you-use.org>`__ checks for a given target.

``VSA``
  Disable all `Visual Studio <https://visualstudio.microsoft.com>`__ checks for a given target.

Examples
--------

Disable all available analysis for a specific target:

.. code-block:: cmake

   cmopts_test_disable_analysis(target1 ALL)
   # equals to
   cmopts_test_disable_analysis(target1
     CLANG_TIDY
     CPPCHECK
     IWYU
     VSA)
