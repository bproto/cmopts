################
cmopts_add_tests
################

Add a subdirectory which contains tests to the build if :ref:`CMOPTS_ENABLE_TESTS <cmopts-options>` is set to ``ON``.

.. code-block:: cmake

   cmopts_add_tests([dir])

Arguments
---------

By default, the directory with ``tests`` name will be used.
The ``dir`` argument can be used to specify other directory which contains tests.
