####################
Module documentation
####################

These documents cover in-detail all public functions and configuration options of ``cmopts`` package.

.. toctree::
   :hidden:
   :maxdepth: 1

   options.rst
   cmopts_add_tests.rst
   cmopts_setup_coverage.rst
   cmopts_setup_target.rst
   cmopts_setup_test.rst
   cmopts_target_disable_analysis.rst
   cmopts_target_link_system_libraries.rst
   cmopts_test_disable_analysis.rst

:doc:`options`
    A list of all available configuration options of ``cmopts`` package.

A list of all public functions of ``cmopts`` package:

- :doc:`cmopts_add_tests`,
- :doc:`cmopts_setup_coverage`,
- :doc:`cmopts_setup_target`,
- :doc:`cmopts_setup_test`,
- :doc:`cmopts_target_disable_analysis`,
- :doc:`cmopts_target_link_system_libraries`,
- :doc:`cmopts_test_disable_analysis`.
