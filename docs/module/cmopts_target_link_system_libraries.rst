###################################
cmopts_target_link_system_libraries
###################################

Link multiple library targets as system libraries (which suppresses their warnings).

.. code-block:: cmake

   cmopts_target_link_system_libraries(<target>
                                       <PRIVATE|PUBLIC|INTERFACE> <item>...
                                       [<PRIVATE|PUBLIC|INTERFACE> <item>...]...)

.. _target_link_libraries: https://cmake.org/cmake/help/latest/command/target_link_libraries.html

The function accepts the same arguments as `target_link_libraries`_.

.. note:: It is worth to mention that this method is implemented on `aminya's project_options CMake module <https://github.com/aminya/project_options>`__ side and ``cmopts`` package simply aliases ``target_link_system_libraries`` method.

Arguments
---------

.. _add_executable: https://cmake.org/cmake/help/latest/command/add_executable.html
.. _add_library: https://cmake.org/cmake/help/latest/command/add_executable.html
.. _ALIAS: https://cmake.org/cmake/help/latest/manual/cmake-buildsystem.7.html#alias-targets

The named ``<target>`` must have been created by a command such as `add_executable`_ or `add_library`_ and must not be an `ALIAS`_ target.

The ``PUBLIC``, ``PRIVATE`` and ``INTERFACE`` scope keywords can be used to specify both the link dependencies and the link interface in one command.

Libraries and targets following ``PUBLIC`` are linked to, and are made part of the link interface.
Libraries and targets following ``PRIVATE`` are linked to, but are not made part of the link interface.
Libraries following ``INTERFACE`` are appended to the link interface and are not used for linking ``<target>``.

Output
------

The include directories of the library are included as ``SYSTEM`` to suppress their warnings.
