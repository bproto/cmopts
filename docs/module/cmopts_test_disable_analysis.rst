############################
cmopts_test_disable_analysis
############################

Disable requested analysis for a given test.

.. code-block:: cmake

   cmopts_test_disable_analysis(<target> <tools...>)

Arguments
---------

.. _add_executable: https://cmake.org/cmake/help/latest/command/add_executable.html
.. _add_library: https://cmake.org/cmake/help/latest/command/add_executable.html
.. _ALIAS: https://cmake.org/cmake/help/latest/manual/cmake-buildsystem.7.html#alias-targets

The named ``<target>`` must have been created by a command such as `add_executable`_ or `add_library`_ and must not be an `ALIAS`_ target.

If ``ALL`` is passed as ``<tools>``, all available analysis will be disabled for a given target.

If you want a more granular control over what analysis to be disabled, you can use one or more of the following options for ``<tools>``:

.. _memcheck: https://valgrind.org/docs/manual/mc-manual.html

``MEMCHECK``
  Set ``DISABLE_MEMCHECK`` label to disable `Valgrind <https://valgrind.org>`__'s `MemCheck <memcheck>`__ checks for a given test.

Examples
--------

Disable all available analysis for a specific target:

.. code-block:: cmake

   cmopts_test_disable_analysis(target1 ALL)
   # equals to
   cmopts_test_disable_analysis(target1 MEMCHECK)

Run all tests with memory checks, excluding those ones that disabled them:

.. prompt:: bash

   ctest -LE "^DISABLE_MEMCHECK$" -T memcheck
