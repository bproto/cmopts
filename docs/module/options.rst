.. _cmopts-options:

#######
Options
#######

A list of all available configuration options of ``cmopts`` package.

.. caution::
   Each of the following configuration option must be set before ``find_package(~)`` is called in order to be correctly processed by ``cmopts`` package.

   The best way to do that is via ``-D`` option of ``cmake``, when you perform a configuration step of your CMake project.

It is worth to mention that a list of following options are implemented on `aminya's project_options CMake module <https://github.com/aminya/project_options>`__ side and ``cmopts`` package simply forwards those options to ``project_options`` function:

- ``CMOPTS_DISABLE_EXCEPTIONS``,
- ``CMOPTS_DISABLE_RTTI``,
- ``CMOPTS_ENABLE_CACHE``,
- ``CMOPTS_ENABLE_IWYU``,
- ``CMOPTS_ENABLE_LTO``,
- ``CMOPTS_ENABLE_NATIVE_OPTIMIZATION``,
- ``CMOPTS_ENABLE_SANITIZER_ADDRESS``,
- ``CMOPTS_ENABLE_SANITIZER_LEAK``,
- ``CMOPTS_ENABLE_SANITIZER_MEMORY``,
- ``CMOPTS_ENABLE_SANITIZER_THREAD``,
- ``CMOPTS_ENABLE_SANITIZER_UNDEFINED_BEHAVIOR``,
- ``CMOPTS_ENABLE_TIME_TRACE``.

Build
-----

**CMOPTS_DISABLE_EXCEPTIONS**:BOOL
  Disable exceptions (no-exceptions and no-unwind-tables).

  Defaults to ``OFF``.

**CMOPTS_DISABLE_RTTI**:BOOL
  Disable `RTTI <https://en.wikipedia.org/wiki/Run-time_type_information>`__.

  Defaults to ``OFF``.

**CMOPTS_ENABLE_LTO**:BOOL
  Enable `interprocedural optimization <https://en.wikipedia.org/wiki/Interprocedural_optimization>`__ (IPO/LTO) for all targets.

  Defaults to ``OFF``.

  .. warning:: This doesn't work well with ``clang`` as of now.

**CMOPTS_ENABLE_NATIVE_OPTIMIZATION**:BOOL
  Enable the optimizations specific to the build machine (e.g. SSE4_1, AVX2, etc.).

  Defaults to ``OFF``.

Cache
^^^^^

**CMOPTS_ENABLE_CACHE**:BOOL
  If enabled and the compiler cache program is available, then project will be built using passed compiler cache to speed up rebuilds of C++ components.

  Defaults to ``OFF``.

**CACHE_OPTION**:STRING
  Tell which compiler cache program to use.

  Defaults to ``ccache``.

  Valid values: ``ccache`` and ``sccache``.

  .. note:: Only used if ``CMOPTS_ENABLE_CACHE`` is set to ``ON``.

The size and location of the cache maintained by ``ccache`` can be adjusted via ``CCACHE_MAXSIZE`` and ``CCACHE_DIR`` environment variables.

Linker
^^^^^^

**CMOPTS_LINKER**:STRING
  If enabled and the external linter program is available, then project will be built using passed linter.

  Add ``-fuse-ld={name}`` to the link invocation.

  The possible value depend on your compiler, for clang the value can be an absolute path to your custom linker, otherwise clang will prefix the name with ``ld.`` and apply its usual search.

  Defaults to ``OFF``, which results in linker detected by CMake.

  .. Valid values: ``bfd``, ``gold`` and ``lld``.

  For example to link project with the ``Gold`` linker, cmake can be invoked with ``-DCMOPTS_LINKER=gold``.

Linters
-------

**CMOPTS_ENABLE_CLANG_TIDY**:BOOL
  Enable static analysis with ``clang-tidy`` for all CMake targets.

  .. warning:: This option **MUST NOT** be used with a usual builds, as it only setups CMake project to b used by an external ``clang-tidy`` script.

  Defaults to ``OFF``.

**CMOPTS_ENABLE_COVERAGE**:BOOL
  Enable static analysis with ``gcov`` for all CMake targets.

  Defaults to ``OFF``.

  .. note:: To generate code coverage files for ``gcov`` to work with, you also need to run all available tests.

  .. warning:: This option is only available for ``clang`` and ``gcc`` compilers.

  It is possible to generate an HTML report with the *coverage-gcovr-html* target.

**CMOPTS_ENABLE_IWYU**:BOOL
  Enable static analysis with ``include-what-you-use`` for all CMake targets.

  Defaults to ``OFF``.

**CMOPTS_ENABLE_LWYU**:BOOL
  Enable static analysis with ``link-what-you-use`` for all CMake targets.

  Defaults to ``ON``.

  .. warning::
     For now, it is only supported for ``ELF`` platforms and is only applicable to executable and shared or module library targets.
     This proper3ty will be ignored for any other targets and configurations.

CppCheck
^^^^^^^^

**CMOPTS_ENABLE_CPPCHECK**:BOOL
  Enable static analysis with ``gcov`` for all CMake targets.

  Defaults to ``OFF``.

**CMOPTS_CPPCHECK_SUPPRESSIONS_FILE**:PATH
  Specifies a file containing suppression rules for the ``cppcheck`` tool.

  Defaults to *None*.

  Default search path for suppression files are following: ``./.cppcheck.supp`` and ``./tools/cppcheck.supp``.
  If any of given path is presented in the root of the CMake project.

  .. note:: Only used if ``CMOPTS_ENABLE_CPPCHECK`` is set to ``ON``.

**CMOPTS_CPPCHECK_REPORT_TYPE**:STRING
  Tell the build system what type of report for the ``cppcheck`` results should be generated.

  Defaults to *None*.

  Valid values and targets:

  - *None* (in-build, no target),
  - *CodeQuality* (*cppcheck-codequality* target),
  - *JUnit* (*cppcheck-junit* target),
  - *HTML* (*cppcheck-html* target).

  .. note:: Only used if ``CMOPTS_ENABLE_CPPCHECK`` is set to ``ON``.

Sanitizers
^^^^^^^^^^

.. note::

    Most of the sanitizers are available only when project is compiled with the ``clang`` or ``gcc`` compilers.
    This means that current build system doesn't support sanitizers on systems from Windows family.

**CMOPTS_ENABLE_SANITIZER_ADDRESS**:BOOL
  If enabled then all binaries and tests will be built with an address sanitizer (ASan).

  Defaults to ``OFF``.

**CMOPTS_ENABLE_SANITIZER_LEAK**:BOOL
  If enabled then all binaries and tests will be built with a leak sanitizer (LSan).

  Defaults to ``OFF``.

**CMOPTS_ENABLE_SANITIZER_MEMORY**:BOOL
  If enabled then all binaries and tests will be built with a memory sanitizer (MSan).

  Defaults to ``OFF``.

  .. warning:: This sanitizer doesn't work with *address*, *leak* and *thread* sanitizers.

  .. warning:: Right now ``valgrind`` is used instead of a memory sanitizer as it's much easier to setup.

**CMOPTS_ENABLE_SANITIZER_UNDEFINED_BEHAVIOR**:BOOL
  If enabled then all binaries and tests will be built with a undefined behavior sanitizer (UBSan).

  Defaults to ``OFF``.

**CMOPTS_ENABLE_SANITIZER_THREAD**:BOOL
  If enabled then all binaries and tests will be built with a thread sanitizer (TSan).

  Defaults to ``OFF``.

  .. warning:: This sanitizer doesn't work with *address*, *leak* and *memory* sanitizers.

Valgrind
^^^^^^^^

**CMOPTS_ENABLE_VALGRING**:BOOL
  Enable static analysis with ``valgrind`` for all CMake targets.

  Defaults to ``OFF``.

**CMOPTS_VALGRING_OPTIONS**:STRING
  Defines the set of Valgrind flags to override the default behavior.

  Defaults to ``--leak-check=full --trace-children=yes --track-fds=yes`` and a few other less important settings.

  .. note:: Only used if ``CMOPTS_ENABLE_VALGRING`` is set to ``ON``.

**CTEST_MEMORYCHECK_SUPPRESSIONS_FILE**:PATH
  Specifies a file containing suppression rules for the ``MemoryCheckCommand`` tool.

  Defaults to *None*.

  Default search path for suppression files are following: ``./.valgrind.supp`` and ``./tools/valgrind.supp``.
  If any of given path is presented in the root of the CMake project.

  .. note:: Only used if ``CMOPTS_ENABLE_VALGRING`` is set to ``ON``.

Tests
-----

**CMOPTS_ENABLE_TESTS**:BOOL
  Enable all tests (e.g. benchmarks, component and unit tests).

  Defaults to ``ON``.

  Targets for building tests are generated only if this option is set to ``ON``.

  You can build a specific unit test using the targets defined under *tests* folder.
  (Search for ``cmopts_setup_test`` in the subdirectories of *tests* for a complete list of unit tests.)
