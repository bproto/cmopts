#####################
cmopts_setup_coverage
#####################

.. _gcovr: https://gcovr.com

This function is intended as a convenience for adding a target for generating HTML-formatted coverage report using `gcovr`_ utility.
It aims to provide sensible defaults so that projects can generally just use this method without any arguments.

.. code-block:: cmake

   cmopts_setup_coverage([target]
                         [BASE_DIRECTORY dir]
                         [COMMAND executable]
                         [DEPENDS [depends...]]
                         [EXCLUDE [patterns...]])

The function defines a target for running and collecting code coverage information, builds dependencies, runs the given executable and outputs the HTML-formatted report.

.. note:: The user can set the variable ``GCOVR_ADDITIONAL_ARGS`` to supply additional flags to the ``gcovr`` command.

Arguments
---------

By default, generated target will have ``coverage`` name.
The ``target`` argument can be used to specify other name for the generated target.

The options are:

.. _CMAKE_SOURCE_DIR: https://cmake.org/cmake/help/latest/variable/CMAKE_SOURCE_DIR.html

``BASE_DIRECTORY``
  The base directory of your source files.
  Defaults to `CMAKE_SOURCE_DIR`_.
  File names are reported relative to this root.

.. _CMAKE_BINARY_DIR: https://cmake.org/cmake/help/latest/variable/CMAKE_BINARY_DIR.html

``COMMAND``
  Specify the command-line to generate ``gcov`` output files.
  Defaults to ``ctest -j${PROCESSOR_COUNT}``.

  .. caution:: Specified command-line is always executed in `CMAKE_BINARY_DIR`_ directory.

.. _add_custom_command: https://cmake.org/cmake/help/latest/command/add_custom_command.html
.. _add_custom_target: https://cmake.org/cmake/help/latest/command/add_custom_target.html

``DEPENDS``
  *(Acts the same way as* ``DEPENDS`` *option of* `add_custom_target`_ *command)*

  Reference files and outputs of custom commands created with `add_custom_command`_ command calls in the same directory (``CMakeLists.txt`` file).
  They will be brought up to date when the target is built.

``EXCLUDE``
  Exclude source files that match the given filter(s).

Requirements
------------

.. note:: The executable should always have a ``ZERO`` as exit code otherwise the coverage generation will not complete.

.. warning:: `gcovr`_ utility must be installed before using this function.

Output
------

Generated HTML-formatted coverage report  will reside by this the following path

- ``${PROJECT_BINARY_DIR}/<target>``.
