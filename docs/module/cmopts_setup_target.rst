###################
cmopts_setup_target
###################

This function is intended as a convenience entry point for all functionality provided by ``cmopts`` module.

It aims to perform all heavy lifting for setting up various target compiler definitions, target properties, and enable different tools for static and dynamic analysis of the target.

.. code-block:: cmake

   cmopts_setup_target(<target> [NO_CHECKS] [STRIP])

Arguments
---------

.. _add_executable: https://cmake.org/cmake/help/latest/command/add_executable.html
.. _add_library: https://cmake.org/cmake/help/latest/command/add_executable.html
.. _ALIAS: https://cmake.org/cmake/help/latest/manual/cmake-buildsystem.7.html#alias-targets

The named ``<target>`` must have been created by a command such as `add_executable`_ or `add_library`_ and must not be an `ALIAS`_ target.

If ``NO_CHECKS`` is given then all non-mandatory compilation warnings will be disabled.
See `this CMake file <https://github.com/aminya/project_options/blob/main/src/CompilerWarnings.cmake>`__ for details.

If ``STRIP`` is given then resulting `executable <add_executable_>`_ or `shared library <add_library_>`_ will be `striped <https://en.wikipedia.org/wiki/Strip_(Unix)>`__.

Output
------

The function adjusts ``target`` accordingly to what ``cmopts`` module :ref:`options <cmopts-options>` have been set.

The options, that affect the end result, are:

- :ref:`CMOPTS_DISABLE_EXCEPTIONS <cmopts-options>`,
- :ref:`CMOPTS_DISABLE_RTTI <cmopts-options>`,
- :ref:`CMOPTS_ENABLE_CACHE <cmopts-options>`,
- :ref:`CMOPTS_ENABLE_CLANG_TIDY <cmopts-options>`,
- :ref:`CMOPTS_ENABLE_IWYU <cmopts-options>`,
- :ref:`CMOPTS_LINKER <cmopts-options>`,
- :ref:`CMOPTS_ENABLE_LTO <cmopts-options>`,
- :ref:`CMOPTS_ENABLE_LWYU <cmopts-options>`,
- :ref:`CMOPTS_ENABLE_NATIVE_OPTIMIZATION <cmopts-options>`,
- :ref:`CMOPTS_ENABLE_SANITIZER_ADDRESS <cmopts-options>`,
- :ref:`CMOPTS_ENABLE_SANITIZER_LEAK <cmopts-options>`,
- :ref:`CMOPTS_ENABLE_SANITIZER_MEMORY <cmopts-options>`,
- :ref:`CMOPTS_ENABLE_SANITIZER_THREAD <cmopts-options>`,
- :ref:`CMOPTS_ENABLE_SANITIZER_UNDEFINED_BEHAVIOR <cmopts-options>`,
- :ref:`CMOPTS_ENABLE_TIME_TRACE <cmopts-options>`.

Beside that the function performs other actions that are not configurable yet.
Those actions are:

- enforcing all warnings to be treated as errors,
- enabling all reasonable compiler warnings (`full list <https://github.com/cpp-best-practices/cppbestpractices/blob/master/02-Use_the_Tools_Available.md#compilers>`__).
