#################
cmopts_setup_test
#################

This function is intended as a convenience entry point for functionality, which is solely related to tests part of the CMake project, provided by ``cmopts`` module.

.. code-block:: cmake

   cmopts_setup_test(<target>
                     NAME <name>
                     <BENCHMARKS        |
                      UNIT_TESTS        |
                      COMPONENT_TESTS   |
                      INTEGRATION_TESTS |
                      SYSTEM_TESTS      >)

Arguments
---------

.. _add_executable: https://cmake.org/cmake/help/latest/command/add_executable.html
.. _`Bracket Argument`: https://cmake.org/cmake/help/latest/manual/cmake-language.7.html#bracket-argument
.. _CMP0110: https://cmake.org/cmake/help/latest/policy/CMP0110.html
.. _`Quoted Argument`: https://cmake.org/cmake/help/latest/manual/cmake-language.7.html#quoted-argument

Adds a test called ``<name>``.
The test name may contain arbitrary characters, expressed as a `Quoted Argument`_ or `Bracket Argument`_ if necessary.
See policy `CMP0110`_.

The named ``<target>`` must have been created by a `add_executable`_ command.

The category must be one of the predefined values, which determines the type of the given test.
There are available following test categories:

``BENCHMARKS``
  Formal definition: https://en.wikipedia.org/wiki/Benchmark_(computing)

  These kind of tests aims to test performance of a specific part of the project code.

``UNIT_TESTS``
  Formal definition: https://en.wikipedia.org/wiki/Unit_testing.

  These kind of tests ensure that a single unit of code (a method) works as expected (given an input, it has a predictable output).
  These tests should be isolated as much as possible.

``COMPONENT_TESTS``
  Formal definition: https://en.wikipedia.org/wiki/Black-box_testing.

  These kind of tests treats a component as a black-box and ensure that it works as expected.
  These tests should be isolated as much as possible.

``INTEGRATION_TESTS``
  Formal definition: https://en.wikipedia.org/wiki/Integration_testing.

  These kind of tests ensure that individual parts of the application work well together, without the overhead of the actual app environment (such as GUI).

``SYSTEM_TESTS``
  Formal definition: https://en.wikipedia.org/wiki/System_testing.

  These kind of tests ensure that the application works as expected, with or without the overhead of the actual app environment (such as GUI).

Examples
--------

Apply common test target options and mark test(s) with a specific label:

.. code-block:: cmake

   cmopts_setup_test(target1
     NAME
       "Target1::Benchmarks"
     BENCHMARKS
   )

Run all tests:

.. prompt:: bash

   ctest

Run all tests that are a part of specific category:

.. prompt:: bash

   ctest -L "^BENCHMARKS$" -T memcheck

Or run all tests except benchmarks:

.. prompt:: bash

   ctest -LE "^BENCHMARKS$" -T memcheck
   ctest -L "_TESTS$" -T memcheck
