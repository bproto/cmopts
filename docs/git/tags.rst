===================
Commit Message Tags
===================

A list of all available tags for commit messages.

Meta Tags
=========

Here is collected a full list of all available *meta* tags for commit messages.

Please take into account that these tags unlike others have a generalized meaning.
That why it's hard to collect all possible cases when a particular tag should be used.

.. table::
    :widths: 60 10 60

    +---------------------------+--------+-----------------------------------+
    | Name                      | Active | Patterns (most common ones)       |
    +===========================+========+===================================+
    | CI                        | Yes    | ``.gitlab-ci.yml``                |
    +---------------------------+--------+-----------------------------------+
    | CMake                     | Yes    | ``*/CMakeLists.txt``              |
    |                           |        |                                   |
    |                           |        | ``**/*.cmake``                    |
    +---------------------------+--------+-----------------------------------+
    | Conan                     | **No** | ``cmake/Conan.cmake``             |
    |                           |        |                                   |
    |                           |        | ``module/Conan.cmake``            |
    +---------------------------+--------+-----------------------------------+
    | Docs                      | Yes    | ``docs/*``                        |
    +---------------------------+--------+-----------------------------------+
    | Example                   | Yes    | ``example/*``                     |
    +---------------------------+--------+-----------------------------------+
    | Sanitizers                | Yes    |                                   |
    +---------------------------+--------+-----------------------------------+
    | VS Code                   | Yes    | ``.vscode/*``                     |
    +---------------------------+--------+-----------------------------------+
    | .. centered:: **Compilers**                                            |
    +---------------------------+--------+-----------------------------------+
    | MSVC                      | Yes    |                                   |
    +---------------------------+--------+-----------------------------------+
    | .. centered:: **Linters**                                              |
    +---------------------------+--------+-----------------------------------+
    | Linters                   | Yes    | ``module/linters/*``              |
    +---------------------------+--------+-----------------------------------+
    | ClangTidy                 | Yes    |                                   |
    +---------------------------+--------+-----------------------------------+
    | CodeCoverage              | Yes    | ``module/coverage/*``             |
    +                           +--------+                                   +
    | Coverage                  | Yes    |                                   |
    +---------------------------+--------+-----------------------------------+
    | CppCheck                  | Yes    | ``module/linters/cppcheck/*``     |
    +---------------------------+--------+-----------------------------------+
    | Valgrind                  | Yes    | ``module/linters/Valgrind.cmake`` |
    +---------------------------+--------+-----------------------------------+
    | .. centered:: **Operating Systems**                                    |
    +---------------------------+--------+-----------------------------------+
    | Linux                     | Yes    |                                   |
    +---------------------------+--------+-----------------------------------+
    | Windows                   | Yes    |                                   |
    +---------------------------+--------+-----------------------------------+
